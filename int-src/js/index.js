import GithubSearch from "./GithubSearch";

// Check the dom for the releavtn container -
const githubForms = document.querySelector(".github-form");

// If it exists, create an instance of githubSearch
if (githubForms) {
  const githubSearchInstance = new GithubSearch();
  githubSearchInstance.init();
} else {
  console.log("No github search form on this page.");
}
