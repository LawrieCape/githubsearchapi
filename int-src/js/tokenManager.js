import { toArray } from "./helpers/toArray";

export default function(passedToken, passedSearchForm) {
  const token = passedToken;
  const extendedApiStatus = !!token;
  const searchForm = passedSearchForm;

  setActiveSelectOptions();
  showRelevantPageElements();
  addTokenButtonListeners();

  // Show only the options in the select box,
  // That can be accessed without the authorisation token.
  function setActiveSelectOptions() {
    const options = searchForm.querySelectorAll("option[data-auth-required]");
    options.forEach(option => {
      option.disabled = !extendedApiStatus;
    });
  }

  // Show / hide sections of the page based on the presence of a token -
  function showRelevantPageElements() {
    const dependantElements = document.querySelectorAll(
      "[data-token-dependant]"
    );
    const independantElements = document.querySelectorAll(
      "[data-token-independant]"
    );

    dependantElements.forEach(element => {
      element.setAttribute("aria-hidden", !extendedApiStatus);
    });

    independantElements.forEach(element => {
      element.setAttribute("aria-hidden", extendedApiStatus);
    });
  }

  // Let the user add thier token via a prompt called from a button-
  function addTokenButtonListeners() {
    const authButtons = toArray(
      document.querySelectorAll('[data-button-function="auth"]')
    );

    authButtons.forEach(button => {
      button.addEventListener("click", promptForToken);
    });
  }

  function promptForToken() {
    const userInputToken = prompt("Please enter your token below.");

    // If they enter a value, udpate the query string.
    if (userInputToken) {
      window.location.search = `?token=${userInputToken}`;
    }
  }
}
