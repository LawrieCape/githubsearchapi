//
// ES6 Templates for the html items to eb genereatred from the API response data.
//

// Repository details layout -
export const generateRepoTemplates = function(repoData) {
  let markup = [];

  const numRepos = repoData.items.length;
  for (var i = 0; i < numRepos; i++) {
    const repo = repoData.items[i];
    const listItem = document.createElement("li");
    const listItemContents = `    
        <h2>
            <a href=${repo.html_url} target="_blank" rel="noopener noreferrer">
                ${repo.name}
            </a>
        </h2>
        <p class="description">${repo.description}</p>
        <p class="stars">${repo.stargazers_count}⭐s</p>    
        <hr/>
    `;

    listItem.innerHTML = listItemContents;

    markup.push(listItem);
  }

  return markup;
};

// Commit details layout
export const generateCommitTemplates = function(commitData) {
  let markup = [];

  const numCommits = commitData.items.length;
  for (var i = 0; i < numCommits; i++) {
    const commit = commitData.items[i];
    const listItem = document.createElement("li");

    // Author is sometime missing? :(
    if (!commit.author) {
      break;
    }

    const listItemContents = `    
          <h2>
            <a href=${
              commit.author.html_url
            } target="_blank" rel="noopener noreferrer">
                ${commit.author.login}
            </a> 
             -         
            <a href=${
              commit.repository.html_url
            } target="_blank" rel="noopener noreferrer">
                ${commit.repository.name}
            </a>
          </h2>
          <p class="description">
            <a href=${
              commit.html_url
            } target="_blank" rel="noopener noreferrer">
                ${commit.commit.message}
            </a>
          </p>
          <hr/>
          
      `;

    listItem.innerHTML = listItemContents;

    markup.push(listItem);
  }

  return markup;
};

// // Code details layout -
export const generateCodeTemplates = function(codeData) {
  let markup = [];

  const numCodeItems = codeData.items.length;
  for (var i = 0; i < numCodeItems; i++) {
    const codeItem = codeData.items[i];
    const listItem = document.createElement("li");
    const listItemContents = `
          <h2>
              <a href=${
                codeItem.html_url
              } target="_blank" rel="noopener noreferrer">
                  ${codeItem.name}
              </a>
          </h2>
          <p class="description">${codeItem.path}</p>          
          <hr/>
      `;

    listItem.innerHTML = listItemContents;

    markup.push(listItem);
  }

  return markup;
};

// Issues details layout
export const generateIssuesTemplates = function(issuesData) {
  let markup = [];

  const numIssues = issuesData.items.length;
  for (var i = 0; i < numIssues; i++) {
    const issue = issuesData.items[i];

    const listItem = document.createElement("li");
    const listItemContents = `
          <h2>
              <a href=${
                issue.html_url
              } target="_blank" rel="noopener noreferrer">
                ${issue.state === "open" ? "❗" : "✔"}
                ${issue.title}
              </a>

          </h2>
          <h4>
            <a href=${
              issue.user.html_url
            } target="_blank" rel="noopener noreferrer">
              ${issue.user.login}
            </a>
          </h4>
          <p class="body">${issue.body}</p>        
          <hr/>  
      `;

    listItem.innerHTML = listItemContents;

    markup.push(listItem);
  }

  return markup;
};

// User details layout -
export const generateUserTemplates = function(userData) {
  let markup = [];
  const numUsers = userData.items.length;
  for (var i = 0; i < numUsers; i++) {
    const user = userData.items[i];
    const listItem = document.createElement("li");
    const listItemContents = `
          <h2>
              <a href=${
                user.html_url
              } target="_blank" rel="noopener noreferrer">
                  ${user.login}
              </a>
          </h2>
          <a href=${user.html_url} target="_blank" rel="noopener noreferrer">
              <img src="${user.avatar_url} alt="${user.login}'s avatar" />   
          </a>
          <hr/>
      `;

    listItem.innerHTML = listItemContents;

    markup.push(listItem);
  }

  return markup;
};

//Error layout -
export const generateErrorTemplates = function(
  loadedApiData,
  resultType,
  searchString
) {
  let markup = [];

  const listItem = document.createElement("li");
  const listItemContents = `
        <h2>            
          Searching for "${searchString}" in ${resultType} gave no results.
        </h2>
    `;

  listItem.innerHTML = listItemContents;

  markup.push(listItem);

  return markup;
};
