import tokenManager from "./tokenManager";
import {
  generateRepoTemplates,
  generateCommitTemplates,
  generateCodeTemplates,
  generateIssuesTemplates,
  generateUserTemplates,
  generateErrorTemplates
} from "./resultsTemplates";

export default function() {
  //   Consts -
  const searchForm = document.querySelector(".github-form");
  const searchInput = document.querySelector(".github-form__input");
  const serachType = document.querySelector(".github-form__search-type");
  const resultsContainer = document.querySelector(".results-container");
  const urlParams = new URLSearchParams(window.location.search);
  const accessToken = urlParams.get("token");

  //
  // Public functions -
  //
  this.init = function() {
    const t = new tokenManager(accessToken, searchForm);
    searchForm.addEventListener("submit", onFormSubmit);
  };

  //
  // Private functions -
  //
  function onFormSubmit(e) {
    // Stop tyhe form posting -
    e.preventDefault();

    // Look up the values of the relevant fields -
    const currentSearchType = serachType.value;
    const searchString = searchInput.value;

    // Call the api request function,
    // Define success and error behaviour -
    loadApiData(currentSearchType, searchString)
      .then(loadedApiData => {
        populatePage(loadedApiData, currentSearchType, searchString);
      })
      .catch(error => handleError(error));
  }

  function loadApiData(searchType, searchString) {
    // Add the accesstioken to the request if it is present -
    const accessTokenDetils = accessToken ? `&accessToken=${accessToken}` : "";
    // Currently using Fetch for the request - potential to move to Axios if better error handling,
    // Or more advanced functionality is required in the future.

    // Add additional data for code & label search demo
    const additionalSearchParams =
      searchType === "code" || searchType === "labels"
        ? "+in:file+repo:facebook/react"
        : "";

    return fetch(
      `https://api.github.com/search/${searchType}?q=${searchString}${additionalSearchParams}${accessTokenDetils}`,
      {
        headers: {
          // Commits -
          Accept: "application/vnd.github.cloak-preview"

          // Topics -
          // Accept: "application/vnd.github.mercy-preview+json"

          // Labels -
          // Accept: "application/vnd.github.symmetra-preview+json"
        }
      }
    )
      .then(response => response.json())
      .catch(error => handleError(error));
  }

  function populatePage(loadedApiData, currentSearchType, searchString) {
    // Remove any content currently displayed -
    emptyPageContents();

    let responseToShow = currentSearchType;

    // Create a holder for the elements to render -
    let markupFromApi;

    console.log(loadedApiData);

    if (!loadedApiData.items || loadedApiData.items.length < 1) {
      // Show the error if there are no results -
      responseToShow = "error";
    }

    switch (responseToShow) {
      case "repositories":
        markupFromApi = generateRepoTemplates(loadedApiData);
        break;

      case "commits":
        markupFromApi = generateCommitTemplates(loadedApiData);
        break;

      case "code":
        markupFromApi = generateCodeTemplates(loadedApiData);
        break;

      case "issues":
        markupFromApi = generateIssuesTemplates(loadedApiData);
        break;

      case "users":
        markupFromApi = generateUserTemplates(loadedApiData);
        break;

      case "error":
      default:
        markupFromApi = generateErrorTemplates(
          loadedApiData,
          currentSearchType,
          searchString
        );
        break;
    }

    // add the newly created elements and its content into the DOM
    const numItems = markupFromApi.length;

    // Create a document fragment, so we only touch the dom once -
    const fragment = document.createDocumentFragment();

    for (let i = 0; i < numItems; i++) {
      const item = markupFromApi[i];
      fragment.appendChild(item);
    }

    // Add the fragement ot the dom -
    resultsContainer.appendChild(fragment);

  }

  function handleError(errorMessage) {
    alert("Sorry there was an error.");
    console.error(errorMessage);
  }

  function emptyPageContents() {
    resultsContainer.innerHTML = "";
  }
}
