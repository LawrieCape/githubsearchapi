/**
 * Converts an array-like object to an array
 * @param {NodeList} arrayLike
 */
export const toArray = function(arrayLike) {
    return Array.prototype.slice.call(arrayLike, 0);
};