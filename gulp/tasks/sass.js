//  ============================================================================================================
//  Dependencies
//  ============================================================================================================

import gulp from "gulp";
import gutil from "gulp-util";

import sass from "gulp-sass";
import concat from "gulp-concat";

import postcss from "gulp-postcss";
import sourcemaps from "gulp-sourcemaps";
import autoprefixer from "autoprefixer";
import svgfragments from "postcss-svg-fragments";
import newer from "gulp-newer";

//  Check environment
let production = gutil.env.prod;
let maps = gutil.env.maps;

// Set Processors
let processors = [
  autoprefixer({ browsers: ["last 2 versions", "ie >= 9", "ios >= 7"] }),
  svgfragments({ encoding: "base64" })
];

//  Custom error logging
function LOG_ERROR(error) {
  if (error.message) {
    console.log("\n");
    console.log(error.message);
    console.log("\n");
  } else {
    console.log(error);
  }

  this.emit("end");
}

//  ============================================================================================================
//  Tasks
//  ============================================================================================================

// Compile Sass
gulp.task("sass", function compileSass() {

  console.log("SASS - production = " +production)

  return gulp
    .src(["int-src/scss/site.scss"], {
      base: "int-src/scss"
    })
    .pipe(production ? gutil.noop() : newer("sass"))
    .pipe(concat("app.css"))
    .pipe(
      sass({
        sourceMaps: "none",
        precision: 10
      }).on("error", function(error) {
        LOG_ERROR(error);
        this.emit("end");
      })
    )
    .pipe(postcss(processors))
    .on("error", LOG_ERROR)
    .pipe(production ? sourcemaps.init({ loadMaps: false }) : gutil.noop())
    .pipe(
      production
        ? sourcemaps.write("./", { includeContent: true })
        : gutil.noop()
    )
    .pipe(gulp.dest("int-dist/assets/css"));
});
