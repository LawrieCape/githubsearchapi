//  ============================================================================================================
//  Dependencies
//  ============================================================================================================

	import gulp from "gulp";
	import gutil from "gulp-util";

	import assemble from "assemble";

	import rename from "gulp-rename";
	import cached from "gulp-cached";
	import remember from "gulp-remember";

	// Check Enviroment
	var production = (gutil.env.prod);


//  ============================================================================================================
//  Tasks
//  ============================================================================================================
	var app = assemble();

	// Prepare HBS Files
	gulp.task("prepare-assemble", function prepareAssemble(complete) {

    console.log("prepare-assemble - production = " +production)

		app.layouts("int-src/html/layouts/**/*.hbs");
		app.partials("int-src/html/partials/**/*.hbs");
		app.pages("int-src/html/pages/**/*.hbs");
		complete();
	});

	// Render Pages
	gulp.task("assemble", gulp.series("prepare-assemble", function assemble(){

    console.log("assemble - production = " +production)

		return app.toStream("pages")
			.pipe(app.renderFile({ "flatten": false }))
			.pipe(cached("assemble"))
			.on("error", console.log)
			.pipe(rename({ extname: ".html" }))
			.pipe(remember("assemble"))
			.pipe(app.dest("int-dist"));
	}));