//  ============================================================================================================
//  Dependencies
//  ============================================================================================================

	import gulp from "gulp";
	import util from "gulp-util";

	import browsersync from "browser-sync";

	// Check Enviroment
	var production = (util.env.prod);

	//	Reload our browsers
	function reload(complete) {
		browsersync.reload();
		complete();
	}



//  ============================================================================================================
//  Tasks
//  ============================================================================================================

	// Configure browsersync
	gulp.task("browsersync", function(complete){
		if (!production) {
			browsersync.create();
			browsersync.init({
				server: "./int-dist/",
				baseDir: "./int-dist/",
				//online: false
			});
		}
		else {
			complete();
		}
	});


	// Watch Tasks
	gulp.task("watch", function(complete){

		if (!production) {
			// Watch HTML
			gulp.watch(
				"int-src/**/*.hbs",
				gulp.series("assemble", reload)
			);
			// Watch CSS
			gulp.watch("int-src/scss/**/*.scss", gulp.series("sass", reload));
			// Watch Scripts
			gulp.watch("int-src/js/**/*.js", gulp.series("browserify", reload));

		}
		else {
			complete();
		}

	});
