//  ============================================================================================================
//  Dependencies
//  ============================================================================================================

import gulp from "gulp";
import gutil from "gulp-util";

import through2 from "through2";
import stream from "vinyl-source-stream";
import buffer from "vinyl-buffer";

import babelify from "babelify";
import browserify from "browserify";

import uglify from "gulp-uglify";
import sourcemaps from "gulp-sourcemaps";

//  Lean error log
function LOG_ERROR(error) {
  if (error.codeFrame) {
    console.log("\n");
    console.log(error.codeFrame);
    console.log("\n");
    console.log(error.loc);
    console.log(error.filename);
    console.log(error.SyntaxError);
    console.log("\n");
  } else {
    console.log(error);
  }

  //  Important for Browserify
  this.emit("end");
}

//  Check environment
var production = gutil.env.prod;
let maps = gutil.env.maps;

//  ============================================================================================================
//  Tasks
//  ============================================================================================================

// Compile Scripts
gulp.task("browserify", function compileScripts(complete) {

  console.log("browserify - production = " +production)

  var transform = browserify({
    paths: ["int-src/js/objects", "int-src/js/helpers"]
  }).transform(babelify, {
    "presets": [
      [
        "@babel/preset-env",
        {
          "useBuiltIns": "entry"
        }
      ]
    ]
  });

  gulp.src(["int-src/js/index.js"]).pipe(
    through2.obj(
      function write(file, enc, next) {
        transform.add(file.path);
        next();
      },
      function end(next) {
        transform
          .bundle()
          .pipe(stream("app.js"))
          .pipe(production ? buffer() : gutil.noop())
          .pipe(
            production ? sourcemaps.init({ loadMaps: false }) : gutil.noop()
          )
          .pipe(production ? uglify() : gutil.noop())
          .pipe(
            production
              ? sourcemaps.write("./", { includeContent: true })
              : gutil.noop()
          )
          .pipe(gulp.dest("int-dist/assets/js"))
          .on("finish", function() {
            complete();
          });

        next();
      }
    )
  );

  complete();
});
