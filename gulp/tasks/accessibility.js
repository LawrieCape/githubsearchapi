//  ============================================================================================================
//  Dependencies
//  ============================================================================================================

    var gulp = require("gulp");

    var rename = require("gulp-rename");
    var accessibility = require("gulp-accessibility");



//  ============================================================================================================
//  Tasks
//  ============================================================================================================

	gulp.task("accessibility", function() {
	  return gulp.src("int-dist/**/*.html")
	    .pipe(accessibility({  "force": true  }))
	    .on("error", console.log)
	    .pipe(accessibility.report({  "reportType": "txt"  }))
	    .pipe(rename({ "extname": "txt" }))
	    .pipe(gulp.dest("logs"));
	});