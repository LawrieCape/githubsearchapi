//  ============================================================================================================
//  Notes
//  ============================================================================================================

//	Gulp task filenames should match gulp task names within
//	require-dir pattern noted as discouraged for gulp v4

//  ============================================================================================================
//  Task imports
//  ============================================================================================================

import gulp from "gulp";
import require from "require-dir";

// Grab Relevent Tasks
require("./gulp/tasks/", { recurse: true });

//  ============================================================================================================
//  Task chains
//  ============================================================================================================

// Default
gulp.task("default", function defaultTasks(done) {
    gulp.parallel("assemble", "sass", "browserify")(done);
});

// Development
gulp.task(
  "dev",
  gulp.series(
    "clean",
    gulp.parallel("assemble", "sass", "browserify"),
    gulp.parallel("browsersync", "watch")
  )
);
