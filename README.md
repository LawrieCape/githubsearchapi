# Github API Test
##### A test of the Github search api.
Live example hosted on Netlify [here](https://github-api-test.netlify.com/).

## Dependencies

- Install [NodeJS](https://nodejs.org/en/download/current/)

## How to install

```
npm install
```

## Active development
```
npm run dev
```
This compile the source, starts a local webserver and watches the source files for changes.

## Production build

```
npm run prod
```

This runs a production build, minifies CSS & JS and outputs the static files.

This is hooked up to Netlify for continuous, automatic deployments.


## Accessibility review
```
npm run accessibility
```

Run the CLI accessibility tool.

*This test has been added but the feedback has not yet been actioned.*


## Tooling

### Javascript
* Vanilla JS
* ES6
* Babel


### CSS
* Sass
* SassMQ
* Sanitize css
* BEM

### HTML
* Assemble
* Handlebars

### Build tools
* NPM scripts
* Gulp
* Browserify
* Browsersync
* Gulp accessibility

## Enhancements and next steps

Investigation into a better implementation of the API authorisation, if the extended API is required.

Extended API methods (commits, topics & labels) currently need custom API request Accept headers.
These are currenlty commented in and out manually. A short term solution could be to switch this based on the current "searchType", but longer term, something more dynamic would be required.

Similarly, although once an access token has been supplied, all public code should be searchable when using the "code" searchType, the api is returning the message ""Must include at least one user, organization, or repository". Due to this, the React Codebase has been temporarily hard-coded as the repository to search.

The structure and functionality of the "resultsTemplates" could be reworked with less code duplication and better styling.

Add proper, BEM class based css layouts for each response template type.

There is the potential to add sorting options for repository results.
This would probably need to be fresh request to the api, rather than clientside sorting / filtering due to the volume of data.

There is the potential to add pagination for results.

Browser testing is required

The JS and dom structure could be updated to allow multiple instances of the github search from on a page if required.
The dom selectors could be passed in to constructor to scope each instance to a dom element.

These are fairly old build tools, which do not gracefully handle JS & CSS errors. These should be updated.

The build tools currently do not allow Scss files to be imported from the node_modules folder, this should be added.

Re-assess the colour schemes. The pink hover colour on grey does not pass accessibility guidelines.

Change class selectors in Github form to data attributes - to separate styling from functionality.


Add animation in & out effects for api returned items. Either simple CSS transitions, or scripted JS animations using AnimeJS / Greensock.